import "./App.css";
import Home from "./components/Home";
import Footer from "./components/layout/Footer";
import Header from "./components/layout/Header";
// import { Router } from 'react-router-dom';
import { Routes } from "react-router-dom";
import { Route } from "react-router-dom";
// import { BrowserRouter as Router } from 'react-router-dom';
import { HelmetProvider } from "react-helmet-async";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ProductDetail from "./components/product/ProductDetail";
function App() {
  return (
    // <Router>
    <div className="App">
      <HelmetProvider>
        <div className="container container-fluid">
          <Header />
          <ToastContainer theme="dark" />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/product/:id" element={<ProductDetail />} />
          </Routes>
        </div>
        <Footer />
      </HelmetProvider>
    </div>
    // </Router>
  );
}

export default App;
